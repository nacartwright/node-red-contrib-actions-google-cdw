module.exports = function(RED) {
    "use strict";
    function setFollowupEvent(config) {
        RED.nodes.createNode(this, config);
        let node = this;
        this.on('input', (msg, send, done) => {
            var eventName
            var parameters
            var parametersType
            if (msg.followupEvent){
                eventName = msg.followupEvent.name;
                parameters = msg.followupEvent.parameters;
                parametersType = 'string'
            } else {
                eventName = config.eventName;
                parameters = config.parameters;
                parametersType = config.parametersType;
            }
            
            send = send || function() {node.send.apply(node, arguments)};
      
            var params = RED.util.evaluateNodeProperty(parameters, parametersType, node, msg);
           
            msg.conv.followup(eventName, params)
            send(msg);     
            
        })
    }
    RED.nodes.registerType('setFollowupEvent', setFollowupEvent);
    
}

