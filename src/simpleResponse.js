const {SimpleResponse, Suggestions} = require('actions-on-google')
module.exports = function(RED) {
    "use strict";
    function addSimpleResponse(config) {
        RED.nodes.createNode(this, config);
        let node = this;
        this.on('input', (msg, send, done) => {
            send = send || function() {node.send.apply(node, arguments)};
            const text = config.text;
            const textType = config.textType;
            const speech = config.speech;
            const speechType = config.speechType;
            const close = config.close;
            // We want to be able to use templates for our response. This node allows for the simpleResponse to be a template or string.

            var responseText = RED.util.evaluateNodeProperty(text, textType, node, msg);
            var responseSpeech = RED.util.evaluateNodeProperty(speech, speechType, node, msg) || responseText;
            
            if (!close)
                msg.conv.ask(new SimpleResponse({
                    text: responseText,
                    speech: responseSpeech || responseText
                }));
            else
                msg.conv.close(new SimpleResponse({
                    text: responseText,
                    speech: responseSpeech ||  responseText
                }));

            if(!close && config.suggestions){
                msg.conv.ask(new Suggestions(config.suggestions.split(',')));
            }
            send(msg);
            done();
        })
    }
    RED.nodes.registerType('simpleResponse', addSimpleResponse);
    
}