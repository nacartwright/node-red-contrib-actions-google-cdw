module.exports = function(RED) {
    "use strict";
    function actionHandler(config) {
        RED.nodes.createNode(this, config);
        let node = this;
        this.on('input', (msg, send, done) => {
            send = send || function() {node.send.apply(node, arguments)};
            this.actionName = config.actionName;
            if(msg.conv.action != this.actionName) {
                return;
            }
            else {
                send(msg);
            }
        })
    }
    RED.nodes.registerType('action', actionHandler);
    
}